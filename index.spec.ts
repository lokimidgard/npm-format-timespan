
import { formatMsSpanMillitary, formatMsSpanWords, parseMsSpanMillitary, TimeUnit } from './index';
import { assert, expect } from 'chai';
import 'mocha';

const MS_SECOND = 1000
const MS_MINUTE = 60000
const MS_HOUR = 3600000
const MS_DAY = 86400000
const MS_MONTH = 2629800000
const MS_YEAR = 31557600000

describe('formatMsSpanWordy', () => {
  describe('rounds to specified time unit', () => {
    const inputs = [
      { inputs: [300, TimeUnit.Second], expected: '0 seconds' },
      { inputs: [500, TimeUnit.Second], expected: '1 second' },
      { inputs: [MS_MINUTE * 29 + MS_SECOND * 59, TimeUnit.Hour], expected: '0 hours' },
      { inputs: [MS_MINUTE * 30, TimeUnit.Hour], expected: '1 hour' },
      { inputs: [MS_MINUTE * 59 + MS_SECOND * 1, TimeUnit.Hour], expected: '1 hour' },
      { inputs: [MS_MINUTE * 59 + MS_SECOND * 59, TimeUnit.Hour], expected: '1 hour' },
      { inputs: [MS_MINUTE * 60, TimeUnit.Hour], expected: '1 hour' },
      { inputs: [MS_MINUTE * 60 + MS_SECOND, TimeUnit.Hour], expected: '1 hour' },
      { inputs: [MS_HOUR + MS_MINUTE * 30, TimeUnit.Hour], expected: '2 hours' },
      { inputs: [MS_DAY + MS_MINUTE * 30, TimeUnit.Hour], expected: '1 day, 1 hour' },
      { inputs: [MS_DAY + MS_MINUTE * 29, TimeUnit.Hour], expected: '1 day' },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Year],
        expected: '2 years'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Month],
        expected: '1 year, 7 months'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Day],
        expected: '1 year, 6 months, 21 days'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Hour],
        expected: '1 year, 6 months, 20 days, 14 hours'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Minute],
        expected: '1 year, 6 months, 20 days, 13 hours, 31 minutes'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Second],
        expected: '1 year, 6 months, 20 days, 13 hours, 30 minutes, 31 seconds'
      },
    ]
    runTestsFromInput(inputs, formatMsSpanWords)
  })

  describe('calculate time parts', () => {
    const inputs = [
      { inputs: [MS_MINUTE * 59 + MS_SECOND * 59, TimeUnit.Hour], expected: '1 hour' },
      { inputs: [MS_SECOND], expected: '1 second' },
      { inputs: [MS_MINUTE], expected: '1 minute' },
      { inputs: [MS_HOUR], expected: '1 hour' },
      { inputs: [MS_DAY], expected: '1 day' },
      { inputs: [MS_MONTH], expected: '1 month' },
      { inputs: [MS_YEAR], expected: '1 year' },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND + 1],
        expected: '1 year, 1 month, 1 day, 1 hour, 1 minute, 1 second, 1 millisecond'
      },
      {
        inputs: [(MS_YEAR * 2) + (MS_MONTH * 2) + (MS_DAY * 2) + (MS_HOUR * 2) + (MS_MINUTE * 2) + (MS_SECOND * 2) + 2],
        expected: '2 years, 2 months, 2 days, 2 hours, 2 minutes, 2 seconds, 2 milliseconds'
      }
    ]
    runTestsFromInput(inputs, formatMsSpanWords)
  })

  describe('excludes where time parts are 0', () => {
    const inputs = [
      {
        inputs: [MS_YEAR + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND + 1],
        expected: '1 year, 1 day, 1 hour, 1 minute, 1 second, 1 millisecond'
      },
      {
        inputs: [MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND + 1],
        expected: '1 month, 1 day, 1 hour, 1 minute, 1 second, 1 millisecond'
      },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND + 1],
        expected: '1 year, 1 month, 1 day, 1 hour, 1 minute, 1 second, 1 millisecond'
      },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_HOUR + MS_MINUTE + MS_SECOND + 1],
        expected: '1 year, 1 month, 1 hour, 1 minute, 1 second, 1 millisecond'
      },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_DAY + MS_MINUTE + MS_SECOND + 1],
        expected: '1 year, 1 month, 1 day, 1 minute, 1 second, 1 millisecond'
      },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_SECOND + 1],
        expected: '1 year, 1 month, 1 day, 1 hour, 1 second, 1 millisecond'
      },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + 1],
        expected: '1 year, 1 month, 1 day, 1 hour, 1 minute, 1 millisecond'
      },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND],
        expected: '1 year, 1 month, 1 day, 1 hour, 1 minute, 1 second'
      },
      {
        inputs: [MS_YEAR + MS_HOUR + MS_SECOND],
        expected: '1 year, 1 hour, 1 second'
      },
    ]
    runTestsFromInput(inputs, formatMsSpanWords)
  })
});

describe('formatMsSpanMillitary', () => {
  describe('calculate time parts', () => {
    const inputs = [
      { inputs: [MS_SECOND], expected: '01' },
      { inputs: [MS_MINUTE], expected: '01:00' },
      { inputs: [MS_HOUR], expected: '01:00:00' },
      { inputs: [MS_DAY], expected: '01:00:00:00' },
      { inputs: [MS_MONTH], expected: '01:00:00:00:00' },
      { inputs: [MS_YEAR], expected: '01:00:00:00:00:00' },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND + 1, 6, 5],
        expected: '01:01:01:01:01:01'
      },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND + 1, 6, 6],
        expected: '01:01:01:01:01:01:001'
      },
      {
        inputs: [(MS_YEAR * 2) + (MS_MONTH * 2) + (MS_DAY * 2) + (MS_HOUR * 2) + (MS_MINUTE * 2) + (MS_SECOND * 2) + 2, 6, 5],
        expected: '02:02:02:02:02:02'
      }
    ]
    runTestsFromInput(inputs, formatMsSpanMillitary)
  })

  describe('rounds to specific time unit', () => {
    const inputs = [
      { inputs: [MS_SECOND, 4, 5], expected: '00' },
      { inputs: [MS_MINUTE, 4, 5], expected: '01:00' },
      { inputs: [MS_HOUR, 4, 5], expected: '01:00:00' },
      { inputs: [MS_DAY, 4, 5], expected: '01:00:00:00' },
      { inputs: [MS_MONTH, 4, 5], expected: '01:00:00:00:00' },
      { inputs: [MS_YEAR, 4, 5], expected: '01:00:00:00:00:00' },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND + 1, 4, 5],
        expected: '01:01:01:01:01:00'
      },
      {
        inputs: [MS_YEAR + MS_MONTH + (MS_DAY * 20) + MS_HOUR + MS_MINUTE + MS_SECOND + 1, 1, 5],
        expected: '01:02:00:00:00:00'
      },
      {
        inputs: [(MS_YEAR * 2) + (MS_MONTH * 2) + (MS_DAY * 2) + (MS_HOUR * 2) + (MS_MINUTE * 2) + (MS_SECOND * 2) + 2, 4, 5],
        expected: '02:02:02:02:02:00'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Year],
        expected: '02:00:00:00:00:00'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Month],
        expected: '01:07:00:00:00:00'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Day],
        expected: '01:06:21:00:00:00'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Hour],
        expected: '01:06:20:14:00:00'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Minute],
        expected: '01:06:20:13:31:00'
      },
      {
        inputs: [(MS_YEAR * 1) + (MS_MONTH * 6) + (MS_DAY * 20) + (MS_HOUR * 13) + (MS_MINUTE * 30) + (MS_SECOND * 31), TimeUnit.Second],
        expected: '01:06:20:13:30:31'
      },
    ]
    runTestsFromInput(inputs, formatMsSpanMillitary)
  })

  describe('only shows to smallest unit', () => {
    const inputs = [
      { inputs: [MS_SECOND, 4, 2], expected: '00' },
      { inputs: [MS_MINUTE, 4, 2], expected: '00' },
      { inputs: [MS_HOUR, 4, 2], expected: '00' },
      { inputs: [MS_DAY, 4, 2], expected: '01' },
      { inputs: [MS_MONTH, 4, 2], expected: '01:00' },
      { inputs: [MS_YEAR, 4, 2], expected: '01:00:00' },
      {
        inputs: [MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND + 1, 4, 2],
        expected: '01:01:01'
      },
      {
        inputs: [MS_YEAR + MS_MONTH + (MS_DAY * 20) + MS_HOUR + MS_MINUTE + MS_SECOND + 1, 1, 2],
        expected: '01:02:00'
      },
      {
        inputs: [(MS_YEAR * 2) + (MS_MONTH * 2) + (MS_DAY * 2) + (MS_HOUR * 2) + (MS_MINUTE * 2) + (MS_SECOND * 2) + 2, 4, 2],
        expected: '02:02:02'
      }
    ]
    runTestsFromInput(inputs, formatMsSpanMillitary)
  })
})

describe('parseMsSpanMillitary', () => {
  describe('calculate time parts', () => {
    const inputs = [
      { inputs: ['0:001'], expected: 1 },
      { inputs: ['01'], expected: MS_SECOND },
      { inputs: ['01:00'], expected: MS_MINUTE },
      { inputs: ['01:00:00'], expected: MS_HOUR },
      { inputs: ['01:00:00:00'], expected: MS_DAY },
      { inputs: ['01:00:00:00:00'], expected: MS_MONTH },
      { inputs: ['01:00:00:00:00:00'], expected: MS_YEAR },
      {
        inputs: ['01:01:01:01:01:01'],
        expected: MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND
      },
      {
        inputs: ['01:01:01:01:01:01:001'],
        expected: MS_YEAR + MS_MONTH + MS_DAY + MS_HOUR + MS_MINUTE + MS_SECOND + 1
      },
      {
        inputs: ['02:02:02:02:02:02'],
        expected: (MS_YEAR * 2) + (MS_MONTH * 2) + (MS_DAY * 2) + (MS_HOUR * 2) + (MS_MINUTE * 2) + (MS_SECOND * 2)
      },
      {
        inputs: ['02:02:02:02:02:02:002'],
        expected: (MS_YEAR * 2) + (MS_MONTH * 2) + (MS_DAY * 2) + (MS_HOUR * 2) + (MS_MINUTE * 2) + (MS_SECOND * 2) + 2
      }
    ]
    runTestsFromInput(inputs, parseMsSpanMillitary)
  }),
    describe('minimal time unit', () => {
      const inputs = [
        { inputs: ['1', TimeUnit.Milli], expected: 1 },
        { inputs: ['1', TimeUnit.Second], expected: MS_SECOND },
        { inputs: ['1', TimeUnit.Minute], expected: MS_MINUTE },
        { inputs: ['1', TimeUnit.Hour], expected: MS_HOUR },
        { inputs: ['1', TimeUnit.Day], expected: MS_DAY },
        { inputs: ['1', TimeUnit.Month], expected: MS_MONTH },
        { inputs: ['1', TimeUnit.Year], expected: MS_YEAR },

        { inputs: ['1:001', TimeUnit.Milli], expected: 1 + MS_SECOND },
        { inputs: ['1:01', TimeUnit.Second], expected: MS_MINUTE + MS_SECOND },
        { inputs: ['1:01', TimeUnit.Minute], expected: MS_HOUR + MS_MINUTE },
        { inputs: ['1:01', TimeUnit.Hour], expected: MS_DAY + MS_HOUR },
        { inputs: ['1:01', TimeUnit.Day], expected: MS_MONTH + MS_DAY },
        { inputs: ['1:01', TimeUnit.Month], expected: MS_YEAR + MS_MONTH },
      ]
      runTestsFromInput(inputs, parseMsSpanMillitary)
    })
    ,
    describe('default time unit', () => {
      const inputs = [
        { inputs: ['0:001'], expected: 1 },
        { inputs: ['0:01', TimeUnit.Second], expected: MS_SECOND },
      ]
      runTestsFromInput(inputs, parseMsSpanMillitary)
    }),
    describe('errors', () => {
      const inputs = [
        { inputs: ['1:000:001'], expected: /^Could not parse "1:000:001" only digits and collons \(:\) are allowed. The digets must be a pair of 2 \(including leading zeros\). Only the first segment may have any number of digets \(at least 1\) and the last must have 3 digits if smallestUnit is 6$/ },
        { inputs: ['a', TimeUnit.Second], expected: /^Could not parse "a" only digits and collons \(:\) are allowed. The digets must be a pair of 2 \(including leading zeros\). Only the first segment may have any number of digets \(at least 1\) and the last must have 3 digits if smallestUnit is 6$/ },
        { inputs: ['1:00:00', TimeUnit.Month], expected: /^More segments found than expected, ensure you have set the correct smallestUnit. \(found 3 parts expected a maximum of 1\)$/ },
      ]

      runTestsFromInputError(inputs, parseMsSpanMillitary)
    })

})

function runTestsFromInput(inputs: { inputs: any[], expected: any, name?: string }[], fut: Function) {
  inputs.forEach(test => {
    it(test.name ? test.name : `should return ${test.expected} when input ${JSON.stringify(test.inputs)}`, () => {
      //@ts-ignore
      const actual = fut(...test.inputs)
      expect(actual).to.equal(test.expected)
    })
  })
}

function runTestsFromInputError(inputs: { inputs: any[], expected: RegExp, name?: string }[], fut: Function) {
  inputs.forEach(test => {
    it(test.name ? test.name : `should throw when input ${JSON.stringify(test.inputs)}`, () => {
      //@ts-ignore
      assert.throws(() => fut(...test.inputs), test.expected)
    })
  })
}